'use strict';

function MainController($scope) {
    $scope.message = 'Hello world!';
}

MainController.$inject = ['$scope'];

var app = angular.module('app', [])
    .controller('mainController', MainController);
