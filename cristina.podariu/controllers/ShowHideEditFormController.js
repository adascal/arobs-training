(function () {
    'use strict';
 //   var app = angular.module('app', []);

    app.controller('ShowHideEditFormController', function ($scope) {
        $scope.isDisplayed = false;
        $scope.showHideForm = function () {
            $scope.isDisplayed = $scope.isDisplayed ? false : true;
        }
    });
})();