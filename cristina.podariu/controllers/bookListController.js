(function () {
    'use strict';

    app.controller('BookListController', function ($scope, $http) {
        $http.get("../files/bookList.json").success(function (data) {
            $scope.books = data;
        });
    });

    app.controller('BookStockParentController', function ($scope) {
        $scope.item = {status: "available"};

    });

    app.controller('BookStockChildController', function ($scope) {
        $scope.updateBookStock = function (stock) {
            if (stock < 1) {
                $scope.item.status = "unavailable";
            }
            else {
                $scope.item.status = "available";
            }
        }
    });
})();