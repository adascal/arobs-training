(function () {
    'use strict';

    function MainController($scope) {
        // var attached to root scope
        $scope.message = 'Hello world!';
    }

    function FirstController($scope) {
        $scope.msg1 = "hello1";
    }
    function SecondController($scope) {
        $scope.msg2 = "hello2";
    }

    function ParentController($scope) {
        $scope.person = {greeted: false};
    }
    function ChildController($scope) {
        $scope.sayHello = function () {
            $scope.person.name = "Ari Lerner";
            $scope.person.greeted = true;
        }
    }

    function AddingMachine($scope) {
        $scope.counter = 0;
        $scope.add = function (amount) {
            $scope.counter += amount;
        };
        $scope.subtract = function (amount) {
            $scope.counter -= amount;
        };
        $scope.multiply = function (amount) {
            $scope.counter *= amount;
        };
        $scope.divide = function (amount) {
            $scope.counter /= amount;
        };
    }
    function MyController($scope) {
        $scope.clock = {
            now: new Date()
        };
        var updateClock = function () {
            $scope.clock.now = new Date()
        };
        setInterval(function () {
            $scope.$apply(updateClock);
        }, 1000);
        updateClock();
    }

    MainController.$inject = ['$scope'];

    angular.module("app", [])
        .controller('mainController',
            // controller manage the var
                function ($scope) {
                    $scope.message = "Ari";
                    $scope.counter = 0;
                })
                .controller('firstController', FirstController)
                .controller('secondController', SecondController)
                .controller('addingMachine', AddingMachine)
                .controller('MyController', MyController)

                .controller('ParentController', ParentController)
                .controller('ChildController', ChildController)
                ;
        })();

