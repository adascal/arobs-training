(function () {
   'use strict';

    function MainController($scope) {
        $scope.message = 'Hello world!';
    }

    MainController.$inject = ['$scope'];

    angular.module('app', [])
        .controller('mainController', MainController);
})();