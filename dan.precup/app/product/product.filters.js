(function(){
    'use strict';

    angular.module("product.filters", [])
        .filter("inStock", function(){
        return function (val){
            return val ? "In stock" : "Not available";
        }
    })

        .filter('capitalize', function(){
            return function(val){
                return (!!val) ? val.charAt(0).toUpperCase() + val.substr(1).toLowerCase() : '';
            }
        })
        .filter('boolToString', function(){
            return function(val){
                if (val == true){
                    return "Yes";
                }

                return "No";
            }
        })

        .filter('boolToSortOrder', function(){
            return function(val){
                if (val == true){
                    return "Lower to higher";
                }

                return "Higher to lower";
            }
        });

})();