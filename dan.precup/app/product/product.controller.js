(function(){
    'use strict';

    function ProductController($scope, ProductData, $log, $filter){
        var promise = ProductData.getList();

        var orderArr = $filter('orderBy');


        promise.then(
            function(response){
                $scope.products = response.data;
                $scope.order('price');
            },
            function(response){
                $log.warn("There was an error: " + response.status + " (" + response.statusText + ")")
            }
        );

        $scope.order = function(orderBy){
            $scope.orderDescending = $scope.orderBy == orderBy ? !$scope.orderDescending: true;
            $scope.orderBy = orderBy;
            $scope.products = orderArr($scope.products, orderBy, $scope.orderDescending);

        }


        $scope.addProfitMargin = function(product){
            product.price *= 1.1;
        }


    }
    ProductController.$inject = ['$scope', 'ProductData', '$log', '$filter'];
    angular.module("product.controllers", []).controller("ProductController", ProductController);
    
    
})();