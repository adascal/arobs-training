(function(){
    'use strict';
    function ProductData($http){
            return {

                getList: function() {
                  return  $http.get("http://localhost/at/dan.precup/app/data/product.json")
                }
            }

    }
    ProductData.$inject = ['$http'];
    angular.module("product.services", []).factory("ProductData", ProductData);
})();