(function(){
    'use strict';

    angular.module('app', ["product"])

    angular.module("product", ["product.services", "product.controllers", "product.filters"]);

})();