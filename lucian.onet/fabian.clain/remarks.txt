Remarks and advices:
1. Use controllerAs in html
2. Have a separate html for listing the products. Place it under app/views folder. Change index.html so that the new html file from views will be used to list the products. E.g. in index.html <ng-include src="'app/views/yourHtmlFile.html'"></ng-include>
3. Use only one ng-app, they cannot be nested: https://docs.angularjs.org/api/ng/directive/ngApp
4. Use sub-module name convention. For e.g. if your root module is named app, the sub-module is recommended to be named as app.submodulename.
5. Be consistent when naming your files: e.g. if you use product.controller.js then use the same pattern for the other files. Recommended conventions:
	product.controller.js	-	will identify the ProductController
	product.service.js		-	will identify the ProductService
	product.filter.js
	product.module.js
6. Use function closure (IIFE recommended style), will help remove global scope and prevent collisions to production env.
e.g.
	(function() {
		'use strict'
		
		//your controller or service etc.
	})();
7. Play with nested controllers: declare a variable with same name on scope inside each controller, check what is displayed on the view, what if you want to change the value from the view?


Keep up the good work! :)

