/*
/!**
 * Created by clain on 4/28/2016.
 *!/
'use strict';
eventsApp.controller('EventController',
    function EventController($scope) {
        $scope.events = [
            {
                name: "Pink cupcake",
                quantity: 0,
                description: "1st free item",
                imageUrl: 'http://sugarcraftshop.thecakepantry.com/ekmps/shops/thecakepantry/images/white-pearl-swirl-cupcake-wrap-382-p.jpg'

            },
            {
                name: "Chocolate cupcake",
                quantity: 33,
                description: "2nd free item",
                imageUrl: 'http://i.imgur.com/mQ2oCEC.jpg'

            },
            {
                name: "Fruits cupcake",
                quantity: 1,
                description: "3rd free item",
                imageUrl: 'http://cdn-image.myrecipes.com/sites/default/files/styles/420x420/public/image/recipes/su/11/07/vegan-cupcake-su-x.jpg?itok=aZD0aRdo'
            }
        ];
        $scope.quantityUp = function(event){
            event.quantity++;
        };
        $scope.quantityDown = function(event){
            event.quantity--;
        };
    }
);
*/

(function() {
    'use strict';

    function EventController ($scope, $injector) {
        $scope.quantityUp = function(event){
            event.quantity++;
        };
        $scope.quantityDown = function(event){
            event.quantity--;
        };

        (function init(){
            var eventService = $injector.get('eventService');

            var promise = eventService.getEvents();
            promise.then(
                function(response){
                    $scope.events = response.data.events;
                }
            );
        })();
        $scope.SortColumn = "date";
    }

    EventController.$inject = ['$scope', '$injector'];

    angular.module('eventsApp.controllers')
        .controller('EventController', EventController);
})();