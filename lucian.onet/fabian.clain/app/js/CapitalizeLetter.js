(function() {
    function CapitalizeLetter() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    }
    angular.module('eventsApp.controllers')
        .filter('CapitalizeLetter', CapitalizeLetter);
})();
