/**
 * Created by clain on 5/9/2016.
 */
(function(){
    'use strict';
    function EventService($http){
        var eventService = {
            getEvents: getEvents
        };
        return eventService;

        function getEvents() {
            return $http.get("http://localhost:63342/fabian.clain/app/obj.json");
        }
    }
    function CapitalizeText(string){
        return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
    }
    EventService.$inject = ['$http'];
    
    angular.module('eventsApp.services')
        .service('eventService', EventService);
})();

/*
(function(){
    'use strict';

})();*/
