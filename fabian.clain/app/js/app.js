(function() {
    'use strict';
    
    angular.module('eventsApp.controllers', []);
    angular.module('eventsApp.services', []);
    
    angular.module('eventsApp',['eventsApp.controllers',
        'eventsApp.services']);
})();