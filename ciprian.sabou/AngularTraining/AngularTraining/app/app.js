var app = null;
(function () {
    'use strict';

    app = angular.module('app', ['ngRoute']);

   // var scotchApp = angular.module('app', ['ngRoute']);

    app.config(function ($routeProvider) {

        $routeProvider.when('/',
        {
            templateUrl: 'views/Login.html',
            controller: 'LoginController'
        }).when('/Login',
        {
            templateUrl: 'views/Login.html',
            controller: 'LoginController'
        })
            .when('/Index',
        {
            templateUrl: 'views/Index.html',
            controller: 'ProductController'
        }).when('/Edit/:id',
        {
            templateUrl: 'views/Edit.html',
            controller: 'ProductController'
        }).when('/About',
        {
            templateUrl: 'views/About.html',
        }).when('/Contact',
        {
            templateUrl: 'views/Contact.html',
        }).
        otherwise('/Login');
    });

    //init
})();