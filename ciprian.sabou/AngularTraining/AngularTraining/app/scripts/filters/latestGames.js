﻿app.filter('latestGames', function() {
    return function (input,enable) {
        if (enable == false)
            return input;
        var result = [];
        for (var i = 0; i < input.length; i++) {
            var date = new Date(input[i].launchDate);
            if (date.getFullYear() == new Date().getFullYear()) {
                result.push(input[i]);
            }
        }
        return result;
    }
});