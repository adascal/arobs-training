﻿app.filter('capitalize', function () {
    return function (input) {
        var words = input.split(' ');
        for (var i = 0; i < words.length; i++) {
            var updatedWord = words[i];
            updatedWord = updatedWord[0].toUpperCase() + updatedWord.substring(1);
            words[i] = updatedWord;
        }
        var result = words.join(' ');
        return result;
    }
});