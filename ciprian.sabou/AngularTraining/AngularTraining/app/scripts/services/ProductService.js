﻿app.factory('ProductService', function ($http) {

    var products = null;
    var upVoteProduct = function (product) {
        product.likes++;
    };
    var getProduct = function (id) {
        for (var i = 0; i < products.length; i++) {
            if (products[i].id == id) {
                return products[i];
            }
        }
    };

    var getAllProducts = function() {
        if (products != null)
            return products;
    };

    var getAllProductsFromServer = function () {
        console.log("getting data");
        return $http.get('data/products.json').success(function (response) {
            products = response;
        }).error(function(response) {
            console.log("error getting data");
        });
    }

    var save = function (product, id) {
        var originalProduct = getProduct(id);
        //var products = getAllProducts();
        var index = products.indexOf(originalProduct);
        if (index != -1) {
            products[index] = product;
        }
    }

    //var products = [
    //    {
    //        "id": 1,
    //        "name": "Rise of the Tomb Raider",
    //        "type": "adventure",
    //        "imageUrl": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTuYYTPIDlmu-OSesqLrEAkFXObDOFNpxjPYfpOgyGHcsxte5DF",
    //        "likes": 5
    //    },
    //    {
    //        "id": 2,
    //        "name": "Far Cry Primal",
    //        "type": "adventure, shooter",
    //        "imageUrl": "https://upload.wikimedia.org/wikipedia/en/1/18/Far_Cry_Primal_cover_art.jpg",
    //        "likes": 12
    //    },
    //    {
    //        "id": 3,
    //        "name": "FIFA 16",
    //        "type": "sport",
    //        "imageUrl": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR49c3UH__tKBVJydZTwcyYoLvt91HnkLZDjvXw_fdDOuosFqb3Zw",
    //        "likes": 0
    //    }
    //];

    return {
        upVote: upVoteProduct,
        getProduct: getProduct,
        getAllProducts: getAllProducts,
        save: save,
        getAllProductsFromServer: getAllProductsFromServer
    }
});

