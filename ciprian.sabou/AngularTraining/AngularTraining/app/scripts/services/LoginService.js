﻿app.factory('LoginService', function ($rootScope) {
    var isLoggedIn = false;
    var logIn = function (username, password) {
        if (username == 'a' && password == 'a') {
            isLoggedIn = true;
        } else {
            isLoggedIn = false;
        }
        $rootScope.$broadcast('userLogin', isLoggedIn);
    }

    var logout = function () {
        isLoggedIn = false;
        $rootScope.$broadcast('userLogin', isLoggedIn);
    }

  

    return {
        isLoggedIn: isLoggedIn,
        logIn: logIn,
        logout: logout
    };
})