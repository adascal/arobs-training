﻿app.directive('review', function () {
    return function (scope, element, attr) {
        var product = scope.$eval(attr.value);
        var rating = scope.$eval(attr.value).rating;
        if (rating >= 9) {
            element.replaceWith(product.name+ " is a very good game");}
        else if (rating >= 8) {
            element.replaceWith(product.name + " is a good game");
        } else {
            element.replaceWith(product.name + " is a mediocre game");
        }
    }
});