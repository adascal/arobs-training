﻿app.controller('EditProductController', function ($scope, $routeParams, $location, $rootScope, ProductService) {
    $scope.test = "from edit product controller";
    $scope.product = ProductService.getProduct($routeParams.id);
    //ProductService.getProduct($routeParams.id, function (response) {
    //    $scope.product = response;
    //});

    $scope.update = function (product) {
        ProductService.save(product, $routeParams.id);
        $location.path('/Index');
    }
});