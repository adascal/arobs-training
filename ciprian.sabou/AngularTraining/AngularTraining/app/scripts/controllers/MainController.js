﻿function MainController($scope, $location, LoginService) {
    $scope.message = 'Hello world!';
    $scope.isLoggedIn = LoginService.isLoggedIn;

    $scope.$on('userLogin', function(event, data) {
        $scope.isLoggedIn = data;
    });

    $scope.$on('userLoggedIn', function () {
        console.log('a user has logged in from broadcast');
    });

    $scope.$on('productLiked', function () {
        console.log('a product has been liked - mainController');
    });

    $scope.logout=function() {
        LoginService.logout();
        $location.path('/Login');
    }
}

//MainController.$inject = ['$scope'];

app.controller('mainController', MainController);