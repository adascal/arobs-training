﻿app.controller('LoginController', function ($scope, $location, $rootScope, LoginService) {
    $scope.loginModel = { username: '', password: '' };
    $scope.logIn = function (loginModel) {
        LoginService.logIn(loginModel.username, loginModel.password);
        $rootScope.$broadcast('userLoggedIn');
        $location.path('/Index');
    };


});