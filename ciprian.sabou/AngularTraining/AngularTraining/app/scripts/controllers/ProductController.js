﻿app.controller('ProductController', function ($scope, $rootScope, ProductService) {
    var imageDisplay = [];
    var previousColumn = '';

    $scope.test = "last product liked at: ";
    $scope.orderByColumn = 'name';
    $scope.orderByDirection = true;
    $scope.show12Plus = false;
    $scope.filtersDisabled = true;
    $scope.showDetailsRowId = 0;

    if (ProductService.getAllProducts() == null) {
        ProductService.getAllProductsFromServer().then(function (response) {
            $scope.products = ProductService.getAllProducts();
            fillImageDisplay($scope.products);
        });
    } else {
        $scope.products = ProductService.getAllProducts();
        fillImageDisplay($scope.products);
    }

    $scope.editMessage = 'edit message';

    $scope.upVote = function (product) {
        var injector = angular.injector(['ng', 'app']);
        var productService = injector.get('ProductService');
        productService.upVote(product);
        if (product.likes < 10) {
            $rootScope.$broadcast('productLiked');
        } else {
            $rootScope.$emit('productLiked');
        }

        setTimeout(function () {
            var date = new Date();
            $scope.$apply('test="last product liked at: ' + date+' "');
        }, 1000);

    }

    $scope.orderBy = function (columnName) {
        if (previousColumn == columnName) {
            $scope.orderByDirection = !$scope.orderByDirection;
            $scope.orderByColumn = columnName;
        } else {
            $scope.orderByColumn = columnName;
        }
        previousColumn = columnName;
    }

    $scope.toggleFilters=function() {
        $scope.filtersDisabled = !$scope.filtersDisabled;
    }

    $scope.toggleImage=function(id) {
        for (var i = 0; i < imageDisplay.length; i++) {
            if (imageDisplay[i].id == id) {
                imageDisplay[i].display = !imageDisplay[i].display;
            }
        }
    }

    function fillImageDisplay(products) {
        for (var i = 0; i < products.length; i++) {
            imageDisplay.push({ "id": products[i].id, "display": true });
        }
    }

    $scope.toggleRowDetails=function(id) {
        if ($scope.showDetailsRowId != -1) {
            $scope.showDetailsRowId = -1;
        } else {
            $scope.showDetailsRowId = id;
        }
    }
    $scope.$watch('products[0].likes', function (scope, current, previous) {
        if (current >= 10) {
            alert('yay, the game got to ' + current + ' likes');
        }

    }, false);


    $rootScope.$on('productLiked', function () {
        console.log('a product has been liked - ProductController');
    });
    //$scope.$watchCollection('products', function (oldValue, newValue) {
    //    console.log('changed');
    //});
});



