#Tips
1. We're all working in the same repository. Everything is public and everyone can see others work

#Requirements
1. Chrome + Batarang
2. IDE (WebStorm, Atom, Visual Studio Code, or any ide you are used to that has html/js support)
3. Git
4. Node.js
5. Bower

#Presets
1. Clone repository using one of the following options:
    * Install and use SourceTree to clone the repository; "Check out in SourceTree" button will be used when you go to Source and you will click the download button located near master
    * Use git clone <url> in git bash where <url> is the one that can be found on top right of this page, near https (e.g. for my user, in git bash I will type: git clone https://alina_cut@bitbucket.org/adascal/arobs-training.git)
2. Create folder firstName.lastName
3. Copy or repeat content of .example/.empty folder
4. Run "bower install" in command line
5. Commit your changes inside created folder using one of the following options:
    * From SourceTree: commit and push
    * From git bash: 
        * git pull
        * git add .
        * git commit -m "your commit message here"
        * git push
        * (prompt for password)


#Program
##S1 - 25/04 - 29/04
* Data Binding and Your First AngularJS Web Application
* Modules
* Scopes
* Controllers/ ControllerAs
* Expressions

###Goals
1. Understand 2 way data binding and use it in your application
2. Understand the notion of a module in angularjs, create and use modules in your application
3. Work with $scope: keep, access and update data from $scope; understand the lifecycle of a scope
4. Understand the scope of controllers and create controllers for needed functionality in your application
5. Find out the utility of controllerAs and use it in your code
6. Work with expressions
7. Based on the architecture chapter from the provided book, create your own directory structure

###Tasks
1. Start working on your online shop application, using the provided example
2. Keep and display the list of products on UI - work with static data for now
3. Modify product details from form and update the information in the list; use ng-model
4. Create a new module for ng-app and ng-controller
5. Create of 2 controllers, each of them with its own scope
6. Add fields with same name to these 2 controllers, try to access the fields and understand what happens
7. Add methods in the controller that will modify the data in the code and check that changes are visible on UI
8. Check best practices of working with modules, scope and controllers

##S2 - 2/05 - 6/05
* Dependency Injection
* Services, factories
* Promises
* Communicating with the Outside World: XHR and Server-Side Communication - $http

###Goals
1. Read, understand and work with dependency injection in angularjs.
2. Find out how to work with services/factories and their purposes.
3. Read about promises, create and resolve promises
4. Work with $http.get

###Tasks
1. In your created controllers, use inline annotation to inject $scope, $rootScope, any other services that you are using in your project.
2. After making the above changes and test your application, use $inject instead of inline annotation. Check best practices on dependency injection.
3. Create services with single responsabilities for your functionalities logic from previous week. Place them under the services folder.
4. Inject created services to your controllers, move functionalities from controllers to created services. Move also the static data to the services.
5. After testing your code, change services to factories. Check best practices on working with services/factories.
6. Move static data into a json file.
7. Use $http.get to retrieve the data from your created json file, handle success and error (display a console log in case of error).


##S3 - 9/05 - 13/05
* $http.post
* Filters
* Introduction to Directives
* Built-In Directives
* Directives Explained

###Goals
1. The goal of this week is to become familiar with some of the angular filters and to create your first custom filter.
2. Find out more angular directives and how to use them in your application.
3. Create your first directive with its own scope and template.

###Tasks
1. Use angular filters: 
    * currency to format a price
    * number to show only 2 decimals for a price 
    * uppercase for menus/submenus
    * orderBy to order the products based on some criteria (price from lowest to highest, from highest to lowest, alphabetically etc)
    * date filter to format a date from your application (it may be the date when a product will be available, or the current date on the footer, or the date when the order will arrive to the customer etc.)
2. Use $filter service
3. Create a custom filter that will filter the products based on some criteria. 
4. Add a custom filter to capitalize the product names.
5. Use ng directives like: ng-if, ng-show, ng-hide, ng-repeat, ng-model, ng-bind, ng-click, ng-change, ng-init, ng-src, ng-href, ng-class, ng-style, ng-disabled, ng-include, ng-cloak
6. Create your custom directive with its own scope and template for showing one product for example. 

##S4 - 16/05 - 20/05
* Multiple Views and Routing
* Events ($emit, $broadcast, $watch)
* The Digest Loop and $apply

###Goals
1. Learn about navigation in a single page application.
2. Configure and start to use $routeProvider between views navigation.
3. Become familiar with routing events.
4. Pass events up or down the scope chain.
5. Read about digest loop and understand how it is working.

###Tasks
1. Create new pages and split your code based on functionality (e.g. the page that will be opened when you select to edit a product should be separated from the page where you list all your products, the page that will be opened when you want to add a new product should be different than the page where you list your products or create a simple login page that will open the list products page when submitted).
2. When you split the code please reconsider the architecture if needed.
3. Configure $routeProvider and use it in your project to navigate between views. Check Multiple Views and Routing chapter from the book on how to configure this service.
4. Use ng-view into your layout template.
5. Use $routeParams (e.g. when you select to edit a product navigate to a different view and send the id of the product as route param)
6. Add Back button to one of your pages to navigate back to previous view.
7. Use $routeChangeSuccess event to scroll page to top.
8. Use $watch/$broadcast/$emit in your application: e.g. you may have a functionality that will set a watch on a product price and will display a message, or will broadcast a message when price of a product is smaller than a certain value (this is just an example, you can choose another functionality that suits your application)
9. Read about digest loop from the book and use $apply on at least one flow on your application.

##S5 - 23/05 - 27/05
* Forms and validations
* Localization
* Caching
* Optimizing Angular Apps

###Goals
1. Add validations to your application.
2. Configure and use angular-translate.
3. Learn about angular caching.
4. Optimize your application.

###Tasks
1. When adding a new product: make product name required, set a minimum length and a maximum length for it, price should be a decimal number and should be required too, product description should have at least 2 characters length. 
2. Submit form only if it is valid otherwise show validation errors under the inputs.
3. Read how to create your own custom validations and if doable use it in your project.
4. Install angular-translate and use it in your project. Set English as preffered language. Add translations for at least one other language (e.g. Romanian) and switch between English and this language.
5. Use caching through $http.
6. Learn how to create a custom cache and if feasible for your project have your own $cacheFactory that will keep the list of products while moving from one page to another and will get the list from the cache when you come back to initial page.
7. Optimize ng-repeat, optimize filters, install and use bindonce, read about minification.


##S6 - 30/05 - 3/06
* Authentication
* Testing

###Goals
1. Become familiar with client side authentication and protection of resources (read Authentication with AngularJS chapter from Ari Lerner book - first link from resources)
2. Add unit tests and e2e tests to your application.

###Tasks
1. Read about client side authentication with AngularJS in Ari Lerner's book. 
2. In your application, redirect unauthenticated users to login page and show them an error message.
3. Handle redirection on unauthorized page requests and protect your resources by route definition.
4. Read about unit tests and start adding unit tests to your code using Jasmine. Cover at least the adding/removing/loading/updating of products functionalities.
5. Install Protractor in order to add e2e tests to your application. Write at least one e2e test for adding a new product to your store.


#Resources
1. https://mega.nz/#!4kBQEYgD!LP4bzy2dkXM48WTHPI0-NJL4bR0T8oWHkNEeGRUAQZ0
2. https://thinkster.io/a-better-way-to-learn-angularjs
3. https://github.com/airbnb/javascript/tree/master/es5
4. https://github.com/johnpapa/angular-styleguide/tree/master/a1
5. https://www.codeschool.com/courses/shaping-up-with-angular-js