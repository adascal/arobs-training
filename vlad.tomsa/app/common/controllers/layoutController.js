/**
 * Created by Vlad Tomsa on 5/8/2016.
 */
(function () {
    'use strict';

    const IS_ADMIN = true;
    const CURRENCY = 'RON';

    function LayoutController($rootScope, $scope, $injector, $filter, $location,  $mdMedia, $mdSidenav, $translate) {
        $scope.changeLanguage = function(languageKey) {
            $translate.use(languageKey);
        };

        $scope.hideMenuItemView = function() {
            $scope.selectedItem = undefined;
        };

        $scope.toggleSidenav = function() {
            $mdSidenav('left').toggle();
        };

        $scope.goHome = function() {
            $location.path('/');
            $location.replace();
        };

        (function init() {
            const layoutService = $injector.get('layoutService');
            $rootScope.$mdMedia = $mdMedia;
            $rootScope.isAdmin = IS_ADMIN;
            
            $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {

                window.scrollTo(0, 0);

            });

            $scope.location = $filter('uppercase')('Cluj Napoca');
            $scope.currentCurrency = CURRENCY;
            layoutService.getMenuItems()
                .then(function(menuItems) {
                    $scope.menuItems = menuItems;
                });
        })();
    }

    LayoutController.$inject = ['$rootScope', '$scope', '$injector', '$filter', '$location',  '$mdMedia', '$mdSidenav', '$translate'];


    angular.module('trainingShop.common.controllers')
        .controller('LayoutController', LayoutController);
})();
