/**
 * Created by Vlad Tomsa on 5/8/2016.
 */
(function() {
    'use strict';

    angular.module('trainingShop.common',[
        'trainingShop.common.controllers',
        'trainingShop.common.services',
        'trainingShop.common.directives',
        'trainingShop.common.config'
    ]);

    angular.module('trainingShop.common.controllers', []);
    angular.module('trainingShop.common.services', []);
    angular.module('trainingShop.common.directives', []);
    angular.module('trainingShop.common.config', []);
})();