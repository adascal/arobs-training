/**
 * Created by Vlad Tomsa on 5/8/2016.
 */
(function() {
    'use strict';
    
    function LayoutService($http, $q) {
        const dataHeaderUrl = ' http://localhost:63342/arobs-training/vlad.tomsa/assets/data';
        const layoutService = {
            getMenuItems: getMenuItems
        };
        return layoutService;
        ///////////////////////

        function getMenuItems() {
            const deferred = $q.defer();
            
            $http({
                method: 'GET',
                url: dataHeaderUrl + '/sidenav_menu.json'
            })
                .success(function(response) {
                    if (response.menuItems) {
                        const menuItems = response.menuItems
                        deferred.resolve(menuItems)
                    }
                    else {
                        deferred.reject(response);
                    }
                })
                .error(function(response) {
                    deferred.reject(response);
                });
            
            return deferred.promise;
        }
    }

    LayoutService.$inject = ['$http', '$q'];

    angular.module('trainingShop.common.services')
        .service('layoutService', LayoutService);
})();