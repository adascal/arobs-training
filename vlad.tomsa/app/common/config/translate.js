/**
 * Created by Vlad Tomsa on 5/29/2016.
 */
(function() {
    'use strict';

    function TranslationConfig($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'assets/data/languages/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
    }

    TranslationConfig.$inject = ['$translateProvider'];

    angular.module('trainingShop.common.config')
        .config(TranslationConfig);
})();