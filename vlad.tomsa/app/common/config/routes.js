/**
 * Created by Vlad Tomsa on 5/28/2016.
 */
(function () {
    'use strict'
    function RoutesConfig($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/common/templates/banner.html'
            })
            .when('/menu', {
                controller: 'MenuController',
                constrollerAs: 'mCtrl',
                templateUrl: 'app/menu/templates/menu.html'
            })
            .when('/menu/:itemId', {
                controller: 'MenuItemController',
                controllerAs: 'ctrl',
                templateUrl: 'app/menu/templates/menuItemProducts.html'
            })
            .otherwise({ redirectTo: '/' });
    }

    RoutesConfig.$inject = ['$routeProvider', '$locationProvider'];

    angular.module('trainingShop.common.config')
        .config(RoutesConfig)
})();
