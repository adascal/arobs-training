/**
 * Created by Vlad Tomsa on 4/30/2016.
 */
(function () {
    'use strict';

    function ThemeConfig($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('cyan', {
                'default': '800',
                'hue-1': '500'
            })
            .accentPalette('orange');
           /* .backgroundPalette('blue-grey');*/
    }

    ThemeConfig.$inject = ['$mdThemingProvider'];
    
    angular.module('trainingShop.common.config')
        .config(ThemeConfig);
})();
