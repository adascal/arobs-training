/**
 * Created by Vlad Tomsa on 5/15/2016.
 */
(function() {
    const ORDER_BY_OPTIONS = [{
        name: 'NAME',
        value: 'name'
    }, {
        name: 'PRICE',
        value: 'price'
    }, {
        name: 'DATE_ADDED',
        value: 'dateAdded'
    }];

    const SORT_OPTIONS = [{
        name: 'Ascending',
        value: true
    }, {
        name: 'Descending',
        value: false
    }];

    function OrderBySelect() {
        return {
            restrict: 'E',
            link: function(scope) {
                scope.orderByOptions = ORDER_BY_OPTIONS;
            },
            templateUrl: 'app/common/directives/templates/orderBySelect.html'
        };
    }

    angular.module('trainingShop.common.directives')
        .directive('orderBySelect', OrderBySelect);
})();