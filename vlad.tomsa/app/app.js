(function () {
   'use strict';

    angular.module('trainingShop', [
        'ngMaterial',
        'ngAnimate',
        'ngMessages',
        'ngRoute',
        'pascalprecht.translate',

        'trainingShop.common',
        'trainingShop.menu'
    ]);
})();