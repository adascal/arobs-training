/**
 * Created by Vlad Tomsa on 5/15/2016.
 */
(function() {
    function MenuItemCapitalizeFilter() {
        return function(input) {
// input will be the string we pass in
            if (input)
                return input[0].toUpperCase() +
                    input.slice(1);
        }
    }
    angular.module('trainingShop.menu.filters')
        .filter('menuItemCapitalizeFilter', MenuItemCapitalizeFilter);
})();
