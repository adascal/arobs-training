/**
 * Created by Vlad Tomsa on 5/1/2016.
 */
(function() {
    'use strict';

    angular.module('trainingShop.menu',[
        'trainingShop.menu.controllers',
        'trainingShop.menu.services',
        'trainingShop.menu.filters'
    ]);

    angular.module('trainingShop.menu.controllers', []);
    angular.module('trainingShop.menu.services', []);
    angular.module('trainingShop.menu.filters', []);
})();
