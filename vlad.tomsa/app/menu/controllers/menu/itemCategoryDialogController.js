/**
 * Created by Vlad Tomsa on 5/8/2016.
 */
(function() {
    'use strict'

    function ItemCategoryDialogController($scope, $mdDialog, productItemInfo) {
        var _productItemInfo = angular.copy(productItemInfo);
        $scope.productItemInfo = productItemInfo;

        $scope.submit = function () {
            const itemDialogForm  = $scope.itemDialogForm;

            if (itemDialogForm.$valid) {

                $mdDialog.hide();
            }
        };

        $scope.reset = function () {
            $scope.productItemInfo = _productItemInfo;
        };
    }

    ItemCategoryDialogController.$inject = ['$scope', '$mdDialog', 'productItemInfo'];

    angular.module('trainingShop.menu.controllers')
        .controller('ItemCategoryDialogController', ItemCategoryDialogController);
})();
