/**
 * Created by Vlad Tomsa on 5/1/2016.
 */
(function() {
    'use strict';
    
    function MenuController($rootScope, $scope, $injector, $mdDialog) {
        const menuService = $injector.get('menuService');
        const isAdmin = $rootScope.isAdmin;

        $scope.createMenuProduct = function() {
            if (isAdmin) {
                const productLength = $scope.menuItems.length - 1;
                const newId = $scope.menuItems[productLength].id + 1;
                const menuItemInfo = {
                    id: newId,
                    name: '',
                    imageUrl: ''
                };
                $mdDialog.show({
                        clickOutsideToClose: true,
                        templateUrl: 'app/menu/templates/menuDialog.html',
                        controller: 'MenuDialogController',
                        controllerAs: 'ctrl',
                        locals: {
                            menuItemInfo: menuItemInfo
                        }
                    })
                    .then(function() {
                        $scope.menuItems.push(menuItemInfo);
                    });
            }
        };

        $scope.removeMenuProduct = function(index) {
            if (isAdmin) {
                const products = $scope.menuItems;
                const dialogContent = 'Are you sure you want to remove ' + products[index].name + '?';

                var confirm = $mdDialog.confirm()
                    .content(dialogContent)
                    .ok('OK')
                    .cancel('Cancel');

                $mdDialog.show(confirm)
                    .then(function() {
                        /* Here we need a $http delete to remove selected element from our list*/
                        products.splice(index,1);
                    })
                    .finally(function() {
                        confirm = undefined;
                    });
            }
        };

        $scope.updateMenuProduct = function(index) {
            if (isAdmin) {
                const products = $scope.menuItems;
                const menuItemInfo = angular.copy(products[index]);
                $mdDialog.show({
                        clickOutsideToClose: true,
                        templateUrl: 'app/menu/templates/menuDialog.html',
                        controller: 'MenuDialogController',
                        controllerAs: 'ctrl',
                        locals: {
                            menuItemInfo: menuItemInfo
                        }
                    })
                    .then(function() {
                        /* Here we need a $http post to add selected element from our list*/
                        products[index] = menuItemInfo;
                    });
            }
        };
        
        (function init() {
            menuService.getMenuItems()
                .then(function(menuItems) {
                    /* Lesson 1 -  Add fields with same name to these 2 controllers, try to access the fields and
                     understand what happens*/
                    $scope.menuItems = menuItems;
                });
        })();
    }

    MenuController.$inject = ['$rootScope', '$scope', '$injector', '$mdDialog'];

    angular.module('trainingShop.menu.controllers')
        .controller('MenuController', MenuController);
})();
