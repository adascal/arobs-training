/**
 * Created by Vlad Tomsa on 5/29/2016.
 */
(function() {
    'use strict';
    const INIT_ORDER_CRITERIA = 'name';
    
    function MenuItemController($rootScope, $scope, $injector, $location, $mdDialog, $routeParams) {
        const menuService = $injector.get('menuService');
        const isAdmin = $rootScope.isAdmin;

        $scope.createProductItem = function() {
            if (isAdmin) {
                const productsList = $scope.selectedMenuItemProduct.productsList;
                const productsLength = productsList.length - 1;
                const newId = productsList[productsLength] ? productsList[productsLength].id + 1 : 0;
                const productItemInfo = {
                    id: newId,
                    name: '',
                    imageUrl: '',
                    description: '',
                    quantity: '',
                    price: ''
                };
                $mdDialog.show({
                        clickOutsideToClose: true,
                        templateUrl: 'app/menu/templates/itemCategoryDialog.html',
                        controller: 'ItemCategoryDialogController',
                        controllerAs: 'ctrl',
                        locals: {
                            productItemInfo: productItemInfo
                        }
                    })
                    .then(function() {
                        productsList.push(productItemInfo);
                    }); 
            }
        };

        $scope.removeItemCategory = function(item) {
            if (isAdmin) {
                const productsList = $scope.selectedMenuItemProduct.productsList;
                const index = productsList.findIndex(function(productItem) {
                    return productItem.id === item.id;
                });
                const dialogContent = 'Are you sure you want to remove ' + productsList[index].name + '?';

                var confirm = $mdDialog.confirm()
                    .content(dialogContent)
                    .ok('OK')
                    .cancel('Cancel');

                $mdDialog.show(confirm)
                    .then(function() {
                        productsList.splice(index,1);
                    })
                    .finally(function() {
                        confirm = undefined;
                    });
            }
        };

        $scope.updateItemCategory = function(item) {
            if (isAdmin) {
                const productItemInfo = angular.copy(item);

                const productsList = $scope.selectedMenuItemProduct.productsList;
                const index = productsList.findIndex(function(productItem) {
                    return productItem.id === item.id;
                });

                $mdDialog.show({
                        clickOutsideToClose: true,
                        templateUrl: 'app/menu/templates/itemCategoryDialog.html',
                        controller: 'ItemCategoryDialogController',
                        controllerAs: 'ctrl',
                        locals: {
                            productItemInfo: productItemInfo
                        }
                    })
                    .then(function() {
                        productsList[index] = productItemInfo;
                    });   
            }
        };

        $scope.goBack = function() {
            $location.path('/menu');
            $location.replace();
        };

        function addError(reason, message) {
            const error = {
                reason: reason,
                message: message
            };
            $scope.errors.push(error);
        }

        function initializeWatchList() {
            $scope.$watch('selectedMenuItemProduct.productsList',
                function(newVal) {
                    if (newVal.length && $scope.errors.length) {
                        $scope.errors = [];
                    }
                }, true);
        }

        (function init() {
            const itemId = parseInt($routeParams.itemId);
            
            $scope.errors = [];
            $scope.orderByCriteria = INIT_ORDER_CRITERIA;
            $scope.selectedMenuItemProduct = {};
            
            menuService.getMenuItemById(itemId)
                .then(function(item) {
                    $scope.selectedMenuItemProduct = item;
                    $scope.selectedMenuItemProduct.productsList = [];

                    menuService.getMenuItemProducts(itemId)
                        .then(function(itemProducts) {
                            $scope.selectedMenuItemProduct.productsList = itemProducts.map(function (itemProduct) {
                                const date = new Date(itemProduct.dateAdded);
                                itemProduct.dateAdded = date;

                                return itemProduct;
                            });
                        })
                        .catch(function(reason) {
                            addError(reason, 'Products unavailable');
                        });

                    initializeWatchList();
                })
                .catch(function(reason) {
                    addError(reason, 'Currently we cannot edit new menu items, because in our logic we are using getMenuItemById' +
                        ' which returns undefined from static JSON ');
                });
        })();
    }

    MenuItemController.$inject = ['$rootScope', '$scope', '$injector', '$location', '$mdDialog' , '$routeParams'];

    angular.module('trainingShop.menu.controllers')
        .controller('MenuItemController', MenuItemController);
})();