/**
 * Created by Vlad Tomsa on 4/30/2016.
 */
(function() {
    'use strict'

    function MenuDialogController($scope, $mdDialog, menuItemInfo) {
        var _menuItemInfo = angular.copy(menuItemInfo);
        $scope.menuItemInfo = menuItemInfo;

        $scope.submit = function () {
            const menuDialogForm  = $scope.menuDialogForm;
            
            if (menuDialogForm.$valid) {

                $mdDialog.hide();
            }
        };

        $scope.reset = function () {
            $scope.menuItemInfo = _menuItemInfo;
        };
    }

    MenuDialogController.$inject = ['$scope', '$mdDialog', 'menuItemInfo'];

    angular.module('trainingShop.menu.controllers')
        .controller('MenuDialogController', MenuDialogController);
})();