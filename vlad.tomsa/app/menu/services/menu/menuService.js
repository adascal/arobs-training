/**
 * Created by Vlad Tomsa on 5/8/2016.
 */
(function() {
    'use strict';

    function MenuService($http, $q) {
        const dataHeaderUrl = ' http://localhost:63342/arobs-training/vlad.tomsa/assets/data';
        const menuService = {
            getMenuItems: getMenuItems,
            getMenuItemById: getMenuItemById,
            getMenuItemProducts: getMenuItemProducts
        };
        return menuService;

        function getMenuItems() {
            const deferred = $q.defer();

            $http({
                method: 'GET',
                url: dataHeaderUrl + '/menu.json'
            })
                .success(function(response) {
                    if (response.menuItems) {
                        const menuItems = response.menuItems
                        deferred.resolve(menuItems)
                    }
                    else {
                        deferred.reject(response);
                    }
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function getMenuItemById(itemId) {
            const deferred = $q.defer();

            this.getMenuItems()
                .then(function(menuItems) {
                    const item  = menuItems.find(function(menuItem) {
                        return menuItem.id === itemId;
                    });

                    if (item) {
                        deferred.resolve(item)
                    }
                    else {
                        deferred.reject('not found');
                    }
                })
                .catch(function(reason) {
                    deferred.reject(reason);
                });

            return deferred.promise;
        }

        function getMenuItemProducts(itemId) {
            const deferred = $q.defer();

            $http({
                method: 'GET',
                url: dataHeaderUrl + '/products_' + itemId + '.json'
            })
                .success(function(response) {
                    if (response.itemProducts) {
                        const itemProducts = response.itemProducts
                        deferred.resolve(itemProducts)
                    }
                    else {
                        deferred.reject(response);
                    }
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }
    }

    MenuService.$inject = ['$http', '$q'];

    angular.module('trainingShop.menu.services')
        .service('menuService', MenuService);
})();
