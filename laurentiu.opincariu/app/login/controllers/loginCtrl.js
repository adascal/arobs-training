/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function() {
    'use strict';

    function LoginCtrl($rootScope, $scope, $location, $injector) {
        var loginService = $injector.get('LoginService');

        function catchServerError(response) {
            console.log(response);
        }

        $scope.login = function () {
            this.dataLoading = true;
            var username = $scope.login.username;
            var password = $scope.login.password;
            var rememberMe = $scope.login.rememberMe;
            loginService.login(username, password, rememberMe)
                .then(function() {
                    $rootScope.isLoggedIn = true;
                    $location.path('/products');
                })
                .catch(catchServerError);
        };

        (function init() {
            $rootScope.isLoggedIn = false;
        })();

    }

    LoginCtrl.$inject = ['$rootScope', '$scope', '$location', '$injector'];

    angular.module('app.login.controllers')
        .controller('LoginCtrl', LoginCtrl);
})();