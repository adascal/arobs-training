/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function () {
    'use strict';

    function LoginService($http, $q, $injector) {
        const APIUrl = 'http://localhost:63342/laurentiu.opincariu/app/data';

        function login(username, password, rememberMe) {
            const deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: APIUrl + '/loginData.json'

                })
                    .success(function (response) {
                        if (response.success) {
                            deferred.resolve();
                        } else {
                            deferred.reject(response);
                        }
                    })
                    .error(function (response) {
                        deferred.reject(response);
                    });

            return deferred.promise;
        }

        return {
            login: login
        };
    }

    LoginService.$inject = ['$http', '$q', '$injector'];

    angular.module('app.login.services')
        .service('LoginService', LoginService);
})();