/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function () {
    'use strict';

     function ProductsService($http, $q, $injector) {
            const APIUrl = 'http://localhost:63342/laurentiu.opincariu/app/data';

            function getProductInfoList() {
               const deferred = $q.defer();

               $http({
                       method: 'GET',
                       url: APIUrl + '/productData.json'

                   })
                       .success(function (response) {
                                console.log(response);
                           if (response.success && response.productInfoList) {
                               var productInfoList = response.productInfoList;
                               deferred.resolve(productInfoList);
                           }
                           else {
                               deferred.reject(response);
                           }
                       })
                       .error(function (response) {
                           deferred.reject(response);
                       });

               return deferred.promise;
            }

            return {
                getProductInfoList: getProductInfoList
            };
     }

    ProductsService.$inject = ['$http', '$q','$injector'];

    angular.module('app.products.services')
        .service('ProductsService', ProductsService);
})();