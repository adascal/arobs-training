/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function(){
    'use strict';

     angular.module('app.products.filters', [])
             .filter('capitalize', function(){
                 return function(s) {
                   return (angular.isString(s) && s.length > 0) ? s[0].toUpperCase() + s.substr(1).toLowerCase() : s;

             };
     });
})();
