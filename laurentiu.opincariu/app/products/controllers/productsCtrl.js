/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function () {
    'use strict';

     function ProductsCtrl($injector, $scope, $filter) {
           var productsService = $injector.get('ProductsService');

            function catchServerError(response) {
                console.log(response);
            }

            $scope.editProduct = function (product) {
                product.editing = true;
            }

            $scope.doneEditing = function (product) {
                product.editing = false;
            }

            $scope.addProduct = function(newProductInfo){
              newProductInfo.id = $scope.productInfoList.length + 1;
              $scope.productInfoList.push(newProductInfo);
            }

            $scope.updateProduct = function(productInfo){
              console.log(productInfo);
            }

            $scope.deleteProduct = function(productInfo){
                 console.log( $scope.productInfoList);
                var index =  $scope.productInfoList.indexOf(productInfo);
                if (index > -1) {
                    $scope.productInfoList.splice(index, 1);
                }
            }

            $scope.setOrder = function (id, order, reverse) {
                $scope.sorting.id = id;
                $scope.sorting.order = order;
                $scope.sorting.direction = reverse;
            };

            function getProducts() {
                productsService.getProductInfoList()
                .then(function(productInfoList) {
                     $scope.productInfoList = productInfoList;
                }).catch(catchServerError);
            }

            (function init() {
                $scope.sorting = {
                    id: "1",
                    order: "id",
                    direction: false
                };

                $scope.dateOpen = false;
                $scope.$on('dPOpen', function (e, dateOpen) {
                    $scope.dateOpen = dateOpen;
                    $scope.dateTimeOpen = false;
                });

                $scope.timestampDatepickerOptions = {
                    showWeeks: false
                };
                $scope.timestampTimepickerOptions = {
                    showMeridian: false
                };

                getProducts();
            })();
     }

    ProductsCtrl.$inject = ['$injector','$scope', '$filter'];
    angular.module('app.products.controllers').controller('ProductsCtrl', ProductsCtrl);
})();