/**
  * Created by laurentiu.opincariu on 5/29/2016.
  */

 (function() {
     'use strict';

     function ContactCtrl($scope) {
         $scope.message = 'Contact us! JK. This is just a demo.';
     }

     ContactCtrl.$inject = ['$scope'];

     angular.module('app.contact.controllers')
         .controller('ContactCtrl', ContactCtrl);
 })();