/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */

(function () {
    'use strict';

     function UserProductsCtrl($injector, $scope, $filter, $location) {
           var userProductsService = $injector.get('UserProductsService');

            function catchServerError(response) {
                console.log(response);
            }

            $scope.viewOpinions = function(productId){
                 $location.path('/opinions/'+productId);
            }

            $scope.buyProduct = function(productInfo){
                productInfo.quantity = productInfo.quantity - 1;
                if(productInfo.quantity <= 0){
                   $scope.deleteProduct(productInfo);
                }
            }

            function dateBiggerThan(obj) {
                var currentDate = new Date();
                console.log(obj);
                currentDate = currentDate.getFullYear() + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDate.getDate()).slice(-2);
                return obj.discountExpirationDate > currentDate;
            };

            $scope.setOrder = function (id, order, reverse) {
                $scope.sorting.id = id;
                $scope.sorting.order = order;
                $scope.sorting.direction = reverse;
            };

            function getProducts() {
                userProductsService.getProductInfoList()
                .then(function(productInfoList) {
                     $scope.productInfoList = $filter('filter')(productInfoList, dateBiggerThan);
                }).catch(catchServerError);
            }

            (function init() {
                $scope.sorting = {
                    id: "1",
                    order: "id",
                    direction: false
                };

                getProducts();
            })();
     }

    UserProductsCtrl.$inject = ['$injector','$scope', '$filter', '$location'];
    angular.module('app.userproducts.controllers').controller('UserProductsCtrl', UserProductsCtrl);
})();