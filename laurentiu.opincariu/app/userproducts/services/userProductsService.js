/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */

(function () {
    'use strict';

     function UserProductsService($http, $q, $injector) {
            const APIUrl = 'http://localhost:63342/laurentiu.opincariu/app/data';

            function getProductInfoList() {
               const deferred = $q.defer();

               $http({
                       method: 'GET',
                       url: APIUrl + '/productData.json'

                   })
                       .success(function (response) {
                                console.log(response);
                           if (response.success && response.productInfoList) {
                               var productInfoList = response.productInfoList;
                               deferred.resolve(productInfoList);
                           }
                           else {
                               deferred.reject(response);
                           }
                       })
                       .error(function (response) {
                           deferred.reject(response);
                       });

               return deferred.promise;
            }

            return {
                getProductInfoList: getProductInfoList
            };
     }

    UserProductsService.$inject = ['$http', '$q','$injector'];

    angular.module('app.userproducts.services')
        .service('UserProductsService', UserProductsService);
})();