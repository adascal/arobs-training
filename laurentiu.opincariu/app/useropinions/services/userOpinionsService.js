/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */

(function () {
    'use strict';

     function UserOpinionsService($http, $q, $injector) {
            const APIUrl = 'http://localhost:63342/laurentiu.opincariu/app/data';

            function getUserOpinionInfoList() {
               const deferred = $q.defer();

               $http({
                       method: 'GET',
                       url: APIUrl + '/opinionData.json'

                   })
                       .success(function (response) {
                                console.log(response);
                           if (response.success && response.userOpinionInfoList) {
                               var userOpinionInfoList = response.userOpinionInfoList;
                               deferred.resolve(userOpinionInfoList);
                           }
                           else {
                               deferred.reject(response);
                           }
                       })
                       .error(function (response) {
                           deferred.reject(response);
                       });

               return deferred.promise;
            }

            return {
                getUserOpinionInfoList: getUserOpinionInfoList
            };
     }

    UserOpinionsService.$inject = ['$http', '$q','$injector'];

    angular.module('app.useropinions.services')
        .service('UserOpinionsService', UserOpinionsService);
})();