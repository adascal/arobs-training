/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */

(function () {
    'use strict';

     function UserOpinionsCtrl($injector, $scope, $filter, $routeParams) {
           var userOpinionsService = $injector.get('UserOpinionsService');

            function catchServerError(response) {
                console.log(response);
            }

            function getOpinions(productId) {
                userOpinionsService.getUserOpinionInfoList()
                .then(function(userOpinionInfoList) {
                     $scope.userOpinionInfoList = $filter('filter')(userOpinionInfoList, {productId:productId});
                }).catch(catchServerError);
            }

            (function init() {
                getOpinions($routeParams.productId);
            })();
     }

    UserOpinionsCtrl.$inject = ['$injector','$scope', '$filter', '$routeParams'];
    angular.module('app.useropinions.controllers').controller('UserOpinionsCtrl', UserOpinionsCtrl);
})();