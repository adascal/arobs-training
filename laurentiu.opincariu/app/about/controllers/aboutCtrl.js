/**
  * Created by laurentiu.opincariu on 5/29/2016.
  */

 (function() {
     'use strict';

     function AboutCtrl($scope) {
        $scope.message = 'Look! I am an about page.';
     }

     AboutCtrl.$inject = ['$scope'];

     angular.module('app.about.controllers')
         .controller('AboutCtrl', AboutCtrl);
 })();