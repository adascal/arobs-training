/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function() {
    'use strict';

    var app = angular.module('app.directives');

    /* make datepicker */
    var datepickerConfig = {
        formatDay: 'dd',
        formatMonth: 'MM',
        formatYear: 'yyyy',
        showWeeks: false,
        startingDay: 1
    };

    app.directive("datePicker", [ function () {
        return {
            restrict: "EA",
            scope: { modelName: "=", strName: '=', isOpen: '=' },
            controller: function ($scope, $filter, $rootScope) {

                $scope.today = function () {
                    $scope.dt = new Date();
                };
                $scope.today();

                $scope.updateStrName = function () {
                    if ($scope.strName && $scope.strName == '') {
                        var newDate = new Date($scope.modelName);
                        $scope.strName = date_formater('yyyy-MM-dd', newDate);
                    }
                };

                $scope.clear = function () {
                    $scope.dt = null;
                };

                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    if ($scope.isOpen == false) {
                        $scope.opened = true;
                        $scope.isOpen = true;
                        $rootScope.$broadcast('dPOpen', true);
                    }
                };
                $scope.$watch('opened', function (value) {
                    if (value == false && $scope.isOpen == true) {
                        $rootScope.$broadcast('dPOpen', false);
                    }
                });
                $scope.update = function ($event) {
                    $scope.updateStrName();
                };

                $scope.format = 'yyyy-MM-dd';

                $scope.dateOptions = {
                    formatDay: 'dd',
                    formatMonth: 'MM',
                    formatYear: 'yyyy',
                    showWeeks: false,
                    startingDay: 1
                };
                $scope.modelName = $filter('date')($scope.modelName, 'yyyy-MM-dd');
            },
            template: '<input type="text" class="date form-control input-sm" datepicker-popup="{{format}}" ng-model="modelName" is-open="opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" ng-click="open($event)" ng-change="update($event)" />'
        };
    }]);

})();
