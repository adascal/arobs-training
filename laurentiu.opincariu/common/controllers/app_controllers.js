/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */

(function () {
   'use strict';

   function MainController($rootScope, $scope, $injector, $translate) {
       $scope.changeLanguage = function(language) {
          $translate.use(language);
       };

       $scope.isLoggedIn = function() {
            return $rootScope.isLoggedIn;
       };

       (function init() {
            $rootScope.isLoggedIn = false;
       })();
   }

    MainController.$inject = ['$rootScope', '$scope', '$injector', '$translate'];

    angular.module('app.controllers').
        controller('MainController', MainController);

})();