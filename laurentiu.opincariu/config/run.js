/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function() {
    'use strict';

    angular.module('app')
        .run(['$rootScope', '$route', '$routeParams', function ($scope, $route, $routeParams) {
            $scope.$on('$routeChangeSuccess', function (/*scope, next, current*/) {
                $scope.lastRoute = $scope.currentRoute;
                $scope.currentRoute = $route.current;
                $scope.part = $route.current.activetab;
                $scope.routeParams = $routeParams;
            });
        }]);
})();