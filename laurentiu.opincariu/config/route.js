/**
 * Created by laurentiu.opincariu on 4/29/2016.
 */

(function () {
    'use strict';

    angular.module('app')
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                // route for the login page (default)
                .when('/', {
                    redirectTo: '/login'
                })
                // route for the login page
                .when('/login', {
                    controller: 'LoginCtrl',
                    templateUrl: 'layout/login/login.html'
                })

                // route for the user products page
                .when('/products', {
                    controller: 'UserProductsCtrl',
                    templateUrl: 'layout/userproducts/userproducts.html'
                })
                // route for the administrate products page
                .when('/administrate', {
                    controller: 'ProductsCtrl',
                    templateUrl: 'layout/products/products.html'
                })
                // route for the about page
                .when('/about', {
                    controller  : 'AboutCtrl',
                    templateUrl : 'layout/about/about.html',
                })
                // route for the contact page
                .when('/contact', {
                    controller  : 'ContactCtrl',
                    templateUrl : 'layout/contact/contact.html',
                })
                // route for the opinions page
                .when('/opinions/:productId', {
                    controller  : 'UserOpinionsCtrl',
                    templateUrl : 'layout/useropinions/useropinions.html',
                })
                // route for the logout action
                .otherwise({redirectTo: '/'});
        }]);
})();