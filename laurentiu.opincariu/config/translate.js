/**
 * Created by laurentiu.opincariu on 6/5/2016.
 */
(function() {
    'use strict';

    function TranslationConfig($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'app/data/languages/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
    }

    TranslationConfig.$inject = ['$translateProvider'];

    angular.module('app')
        .config(TranslationConfig);
})();