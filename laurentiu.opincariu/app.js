(function () {
   'use strict';

    angular.module('app', [
        'ngRoute',
        'ngMessages',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'pascalprecht.translate',
        'app.controllers',
        'app.directives',
        'app.data',
        'app.login',
        'app.products',
        'app.userproducts',
        'app.useropinions',
        'app.about',
        'app.contact'
    ]);


    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.data', []);

    angular.module('app.login', [
        'app.login.services',
        'app.login.controllers'
    ]);
    angular.module('app.login.services', []);
    angular.module('app.login.controllers', []);

    angular.module('app.products', [
        'app.products.services',
        'app.products.controllers',
        'app.products.filters'
    ]);
    angular.module('app.products.services', []);
    angular.module('app.products.controllers', []);
    angular.module('app.products.filters', []);

    angular.module('app.userproducts', [
        'app.userproducts.services',
        'app.userproducts.controllers',
        'app.userproducts.filters',
        'app.userproducts.directives'
    ]);
    angular.module('app.userproducts.services', []);
    angular.module('app.userproducts.controllers', []);
    angular.module('app.userproducts.filters', []);
    angular.module('app.userproducts.directives', []);

   angular.module('app.useropinions', [
        'app.useropinions.services',
        'app.useropinions.controllers'
    ]);
    angular.module('app.useropinions.services', []);
    angular.module('app.useropinions.controllers', []);

    angular.module('app.about', [
        'app.about.controllers'
    ]);
    angular.module('app.about.controllers', []);

    angular.module('app.contact', [
        'app.contact.controllers'
    ]);
    angular.module('app.contact.controllers', []);
})();